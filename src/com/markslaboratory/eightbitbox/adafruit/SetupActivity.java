package com.markslaboratory.eightbitbox.adafruit;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Set;

public class SetupActivity extends Activity {

    String TAG = "8BitSetup";

    BluetoothService myService;
    ServiceConnection mConnection;
    SharedPreferences myPreferences;
    SharedPreferences.Editor myEditor;
    boolean isBound;

    Dialog initialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        myEditor = myPreferences.edit();

        // We need to bind to our service, so lets go ahead and do that
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                isBound = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(SetupActivity.this);
                builder.setTitle("Set up!");
                builder.setIcon(R.drawable.ic_launcher);
                String msg =
                        "Let's set up your 8BitBox!\n\n" +
                                "(You will need to have both Bluetooth turned on, and the device already paired in the system Bluetooth settings)\n\n" +
                                "Please press the button to continue";
                builder.setMessage(msg);
                builder.setPositiveButton("Let's set it up!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setupWithoutNFC();
                    }
                });
                initialDialog = builder.create();
                initialDialog.setCanceledOnTouchOutside(false);
                initialDialog.show();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                isBound = false;
            }
        };


        myService = new BluetoothService();
        isBound = false;
        Intent myIntent = new Intent(getApplicationContext(), BluetoothService.class);
        bindService(myIntent, mConnection, Context.BIND_AUTO_CREATE);

        setupWithoutNFC();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound) {
            unbindService(mConnection);
        }
    }

    public void genericDialog(String title, String msg) {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(SetupActivity.this);
        builder.setTitle(title);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }



    void setupWithoutNFC() {
        // We assume no NFC functionality, so we show a list of paired BT devices.
        myEditor.putString(BoxConstants.STORED_NFC, BoxConstants.NO_NFC);
        myEditor.commit();
        setupGuest();

    }




    void setupGuest() {

        int nDevices = 0;

        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please pick the 8BitBox from your paired Bluetooth devices:");

        // Set up our ListView to hold the items
        final ListView macList = new ListView(this);

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        // Set up our ArrayAdapter
        final ArrayAdapter<String> macAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1);
        macList.setAdapter(macAdapter);

        // Get the list of paired devices
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        // Add paired devices to the list
        nDevices = pairedDevices.size();
        if (nDevices > 0) {
            for (BluetoothDevice device : pairedDevices) {
                // Add the Name and MAC
                macAdapter.add(device.getName() + "\n" + device.getAddress());
            }

        }


        builder.setView(macList);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        final Dialog finalDialog = dialog;
        macList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {

                // Once an item is selected...

                // Don't let people click it twice
                macList.setClickable(false);

                // Get the MAC address
                String deviceToConnect = macAdapter.getItem(arg2);
                int MACLength = deviceToConnect.length();
                deviceToConnect = deviceToConnect.substring(MACLength - 17, MACLength);

                // Store it
                myEditor.putString(BoxConstants.STORED_MAC, deviceToConnect);
                myEditor.commit();
                finalDialog.dismiss();


            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                // Did we store a MAC
                String testMAC = myPreferences.getString(BoxConstants.STORED_MAC,"");
                String msg = "";
                String title = "";
                if (testMAC.equals("")) {
                    title = "Try Again!";
                    msg = "It seems you didn't store a MAC address. Maybe it wasn't already in your " +
                            "list of paired Bluetooth devices? Please pair it and try again!";
                }
                else {
                    title = "Setup Complete!";
                    msg = "Your 8BitBox should be set up and ready to use!";
                }
                setupFinishedDialog(title, msg);
            }
        });

        dialog.show();
        // Immediately dismiss if there were no paired devices
        if (nDevices == 0) {
            dialog.dismiss();
        }

    }

    void setupFinishedDialog(String title, String msg) {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(SetupActivity.this);
        builder.setTitle(title);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SetupActivity.this.finish();
            }
        });
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
