package com.markslaboratory.eightbitbox.adafruit;


public class BoxConstants {

    // These are commands sent to the Arduino
    static byte[] RED = "R".getBytes();
    static byte[] GREEN = "G".getBytes();
    static byte[] BLUE = "B".getBytes();
    static byte[] MUSIC = "Z".getBytes();
    static byte[] STORE_FAVORITE_COLOR = "L".getBytes();
    static byte[] MUSIC_RESPONSE = "z".getBytes();

    // Shared Preferences Strings
    static String STORED_NFC = "STORED_NFC";
    static String STORED_MAC = "STORED_MAC";
    static String NO_NFC = "NO_NFC";
    static String HAS_FAVORITE = "HAS_FAVORITE";
    static String FAVORITE_RED = "FAVORITE_RED";
    static String FAVORITE_GREEN = "FAVORITE_GREEN";
    static String FAVORITE_BLUE = "FAVORITE_BLUE";
}
